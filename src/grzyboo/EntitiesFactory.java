package grzyboo;

import grzyboo.entities.*;

import java.awt.*;
import java.util.Random;

public class EntitiesFactory {
     public Entity createAsteroid() {
        double hSpeed = Randomizer.nextFloat(100, 350, 5);
        double vSpeed = -Randomizer.nextFloat(100, 350, 5);
        boolean side = Randomizer.nextBoolean();

        double y = Randomizer.nextDouble(0, GameWindow.GAME_SIZE.getHeight() - 100);
        double x;

        double radius = Randomizer.nextInt(30, 70, 10);
        if(side) {
            hSpeed *= -1;
            x = GameWindow.GAME_SIZE.getWidth() + radius;
        }
        else
            x = -radius;

        double angularSpeed = Randomizer.nextInt(-180, 180);

        Asteroid asteroid = new Asteroid(radius);
        asteroid.setPosition(x, y);
        asteroid.setSpeed(hSpeed, vSpeed);
        asteroid.setAngularSpeed(angularSpeed);

        return asteroid;
    }

    class EntityParameters {
        double x;
        double y;
        double speedX;
        double speedY;
        double radius;
    }

    public Entity createAntiCampingAsteroid(PlayerCursor cursor) {
        double angularSpeed = Randomizer.nextInt(-180, 180);

        EntityParameters params = getRandomAsteroidParams(cursor);
        Asteroid asteroid = new Asteroid(params.radius);
        asteroid.setPosition(params.x, params.y);
        asteroid.setSpeed(params.speedX, params.speedY);
        asteroid.setAngularSpeed(angularSpeed);

        return asteroid;
    }

    private EntityParameters getRandomAsteroidParams(PlayerCursor cursor) {
        EntityParameters params = new EntityParameters();

        Random r = new Random();
        params.radius = (r.nextInt(2) + 2) * 10;
        Dimension size = GameWindow.GAME_SIZE;

        // Set X
        if(r.nextBoolean())
            params.x = size.getWidth() + params.radius;
        else
            params.x = -params.radius;

        // Set Y
        params.y = r.nextInt((int)size.getHeight() - 200) + 200;

        double speedY = -200.0f;
        QuadraticEquation equation = new QuadraticEquation(0.5f* Entity.gravityAcceleration, speedY, params.y - cursor.getY());
        while(equation.delta() < 0.0 && speedY > -500.0f) {
            speedY -= 25.0f;
            equation.setB(speedY);
        }
        params.speedY = speedY;

        double timeToReachCursorY = equation.x2();
        params.speedX = (cursor.getX() - params.x) / timeToReachCursorY;

        return params;
    }

    public Entity createPowerUp(PlayerCursor cursor, double playingTime) {
        PowerUp p = null;
        int max = 4;

        // Prevent player from healthPoints farming and stop generating them after 3 minutes entirely.
        if(cursor.isHealthMaximum() || playingTime >= 180.0)
            --max;

        Random r = new Random();
        switch(r.nextInt(max)) {
            case 0:
                p = new PowerSlowDown();
                break;
            case 1:
                p = new PowerBoom();
                break;
            case 2:
                p = new PowerImmortality();
                break;
            case 3:
                p = new PowerHealth();
                break;
        }

        return p;
    }
}
