package grzyboo;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public abstract class Resources {
    public final static SoundEffect MENU = new SoundEffect("sound/menu_change.wav", false);
    public final static SoundEffect MENU_CLICK = new SoundEffect("sound/menu_click.wav", false);

    public final static SoundEffect MUSIC = new SoundEffect("sound/soundtrack.wav", true);
    public final static SoundEffect HIT = new SoundEffect("sound/hit.wav", false);
    public final static SoundEffect GAME_OVER = new SoundEffect("sound/game_over.wav", false);
    public final static SoundEffect PICKUP = new SoundEffect("sound/pickup.wav", false);

    public final static Font FONT = new Font("Arial Black", Font.PLAIN, 50);
    public final static Font DEFAULT_FONT = new JLabel().getFont();

    public static BufferedImage IMG_HEART;
    public static BufferedImage IMG_HP_FILLED;
    public static BufferedImage IMG_HP_EMPTY;

    public static void load() {
        IMG_HEART = tryLoad("images/heart.png");
        IMG_HP_FILLED = tryLoad("images/hp_filled.png");
        IMG_HP_EMPTY = tryLoad("images/hp_empty.png");
    }

    private static BufferedImage tryLoad(String path) {
        try {
            return ImageIO.read(new File(path));
        }
        catch(IOException e) {
            e.printStackTrace();
        }

        return new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
    }
}
