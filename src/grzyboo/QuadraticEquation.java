package grzyboo;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double delta() {
        return b*b - 4*a*c;
    }

    public double x1() {
        double d = delta();
        if(d < 0.0)
            throw new RuntimeException("Delta <= 0 for quadratic equation");

        return (-b - Math.sqrt(d)) / (2 * a);
    }

    public double x2() {
        double d = delta();
        if(d < 0.0)
            throw new RuntimeException("Delta <= 0 for quadratic equation");

        return (-b + Math.sqrt(d)) / (2 * a);
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }
}
