package grzyboo.entities;

import java.awt.*;

public abstract class Entity extends Point2f {
    private double speedX, speedY;
    protected boolean gravity = true;

    public static final double gravityAcceleration = 200.0;

    public abstract void drawOnCanvas(Graphics2D g);
    public abstract boolean shouldDelete();

    public double getSpeedY() {
        return speedY;
    }

    public double getSpeedX() {
        return speedX;
    }

    public void setSpeed(double x, double y) {
        speedX = x;
        speedY = y;
    }

    public void updatePhysics(double timeSinceLastUpdate) {
        x += speedX * timeSinceLastUpdate;
        y += speedY * timeSinceLastUpdate;

        if(gravity)
            speedY += gravityAcceleration * timeSinceLastUpdate;
    }
}
