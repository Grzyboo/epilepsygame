package grzyboo.entities;

import grzyboo.GamePanel;
import grzyboo.SimpleTimer;
import grzyboo.TimersManager;

import javax.swing.*;
import java.awt.event.ActionListener;

public class PowerSlowDown extends PowerUp {
    @Override
    public void onCursorTouch() {
        GamePanel panel = GamePanel.getInstance();
        panel.setSlowDownRatio(0.2);

        SimpleTimer timer = new SimpleTimer(2500, () -> panel.setSlowDownRatio(1.0), false);
        timer.setAbsolute(true);
        panel.addTimer(timer);
    }
}
