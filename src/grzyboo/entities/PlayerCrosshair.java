package grzyboo.entities;

import java.awt.*;

public class PlayerCrosshair extends Point2f {

    public void drawOnCanvas(Graphics2D g) {
        g.setColor(Color.RED);
        g.setStroke(new BasicStroke(3));

        double size = 5;
        double gap = 4;
        drawHorizontalLine(g, x - gap - size,x - gap);
        drawHorizontalLine(g, x + gap,x + gap + size);

        drawVerticalLine(g, y - gap - size,y - gap);
        drawVerticalLine(g, y + gap,y + gap + size);
    }

    void updatePosition(double x, double y) {
        this.x = x;
        this.y = y;
    }

    private void drawHorizontalLine(Graphics2D g, double x1, double x2) {
        g.drawLine((int)x1, (int)y, (int)x2, (int)y);
    }

    private void drawVerticalLine(Graphics2D g, double y1, double y2) {
        g.drawLine((int)x, (int)y1, (int)x, (int)y2);
    }
}
