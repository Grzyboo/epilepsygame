package grzyboo.entities;

import grzyboo.GamePanel;
import grzyboo.GameWindow;
import grzyboo.Resources;
import grzyboo.Settings;
import grzyboo.colors.ColorChanger;
import grzyboo.colors.RandomColorChanger;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class PlayerCursor extends Entity {
    private PlayerCrosshair crosshair;
    private ColorChanger colorChanger;

    private int healthPoints;
    private double radius = 11.0;

    private double immortalityTime;
    private int immortalityDrawOffset;
    private final int maxImmortalityOffset = 200;

    public PlayerCursor() {
        x = GameWindow.GAME_SIZE.getWidth()*0.5;
        y = GameWindow.GAME_SIZE.getHeight()*0.5;

        crosshair = new PlayerCrosshair();
        crosshair.setPosition(x, y);

        colorChanger = new RandomColorChanger(2.0);

        reset();
    }

    public void reset() {
        this.healthPoints = 3;
        immortalityTime = 0.0;
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {
        crosshair.drawOnCanvas(g);

        Ellipse2D shape = new Ellipse2D.Double(x - radius, y - radius, radius*2, radius*2);
        g.setStroke(new BasicStroke(2));
        g.setColor(colorChanger.getColor());
        g.fill(shape);
        g.setColor(Color.LIGHT_GRAY);
        g.draw(shape);

        drawHealthPoints(g);

        if(isImmortal())
            drawImmortalityIndicator(g);
    }

    private void drawHealthPoints(Graphics2D g) {
        int filled = healthPoints;
        int empty = 3 - healthPoints;

        for(int i = 0; i < filled; ++i)
            g.drawImage(Resources.IMG_HP_FILLED, 50*i + 20, 10, null);

        for(int i = 0; i < empty; ++i)
            g.drawImage(Resources.IMG_HP_EMPTY, 50*filled + 50*i + 20, 10, null);
    }

    private void drawImmortalityIndicator(Graphics2D g) {
        Dimension size = GamePanel.getInstance().getSize();
        int panelX = (int)size.getWidth();
        int panelY = (int)size.getHeight();

        double fraction = 1.0 * immortalityDrawOffset / maxImmortalityOffset;
        int alpha = (int)Math.round((1.1 - fraction) * 230);
        Color color = new Color(0, 255, 0, alpha);
        g.setColor(color);

        int strokeWidth = (int)Math.round(20 * fraction);
        g.setStroke(new BasicStroke(strokeWidth));

        int offset = immortalityDrawOffset - strokeWidth/2;
        g.drawLine(0, offset, offset, 0);
        g.drawLine(panelX - offset, 0, panelX, offset);
        g.drawLine(0, panelY - offset, offset, panelY);
        g.drawLine(panelX - offset, panelY, panelX, panelY - offset);

        if(isImmortal()) {
            g.setFont(Resources.FONT);

            String value = String.format("%.1f", immortalityTime);
            FontMetrics fm = g.getFontMetrics();
            int x = (int)((GameWindow.GAME_SIZE.getWidth()-fm.stringWidth(value))*0.5);

            g.drawString(value, x, 100);
        }
    }

    @Override
    public void updatePhysics(double timeSinceLastUpdate) {
        colorChanger.update(timeSinceLastUpdate);

        if(GamePanel.getInstance().shouldMoveCursor()) {
            double angle = vectorAngle(this, crosshair);
            double dist = distance(this, crosshair);

            double maxDist = 1000.0 * timeSinceLastUpdate;
            if (dist > maxDist)
                dist = maxDist;

            x += Math.cos(angle) * dist;
            y += Math.sin(angle) * dist;
        }

        if(isImmortal()) {
            immortalityTime -= timeSinceLastUpdate;

            if(immortalityTime < 0.0)
                immortalityTime = 0.0;

            immortalityDrawOffset = (immortalityDrawOffset + 4) % maxImmortalityOffset;
        }
    }

    @Override
    public boolean shouldDelete() {
        return false;
    }

    @Override
    public void setPosition(double x, double y) {
        crosshair.updatePosition(x, y);
    }

    @Override
    public void setPosition(Point2f p) {
        crosshair.updatePosition(x, y);
    }

    public double getRadius() {
        return radius;
    }

    public Color getColor() {
        return colorChanger.getColor();
    }

    public boolean isHealthMaximum() {
        return healthPoints >= 4;
    }

    void incrementHealth() {
        ++healthPoints;
    }

    public void hit() {
        if(immortalityTime <= 0) {
            --healthPoints;

            setImmortal(1000);

            if (healthPoints <= 0 && !Settings.PERMANENT_IMMORTALITY)
                GamePanel.getInstance().gameOver();

            Resources.HIT.play();
        }
    }

    void setImmortal(int msTime) {
        immortalityTime += msTime * 0.001;
    }

    private boolean isImmortal() {
        return immortalityTime > 0.0;
    }
}
