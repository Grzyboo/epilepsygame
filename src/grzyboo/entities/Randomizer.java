package grzyboo.entities;

import java.util.Random;

/**
 * This class is useful, because when you want to randomize a number in an interval the code looks very messy like:
 * speed = (r.nextInt(50) + 20)*5 means 100-350 with a step of 5
 */
public abstract class Randomizer {
    private static Random random = new Random();

    public static int nextInt(int min, int max) {
        return nextInt(min, max, 1);
    }

    public static int nextInt(int min, int max, int step) {
        return (random.nextInt((max - min)/step + 1) + min/step)*step;
    }

    public static float nextFloat(float min, float max) {
        return nextFloat(min, max, 0.1f);
    }

    public static float nextFloat(float min, float max, float step) {
        return (random.nextInt(Math.round((max - min)/step) + 1) + min/step)*step;
    }

    public static double nextDouble(double min, double max) {
        return nextDouble(min, max, 0.1);
    }

    public static double nextDouble(double min, double max, double step) {
        return (random.nextInt((int)Math.round((max - min)/step) + 1) + min/step)*step;
    }

    public static boolean nextBoolean() {
        return random.nextBoolean();
    }

}
