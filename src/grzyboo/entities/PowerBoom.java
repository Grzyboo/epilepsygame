package grzyboo.entities;

import grzyboo.GamePanel;

public class PowerBoom extends PowerUp {
    public void onCursorTouch() {
        GamePanel.getInstance().makeBoomPowerUp();
    }
}
