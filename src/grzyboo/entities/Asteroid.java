package grzyboo.entities;

import grzyboo.GameWindow;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

public class Asteroid extends Entity {
    private ArrayList<Point2f> points;

    private double angularSpeed;
    private double angle;
    private double radius;

    public Asteroid(double radius) {
        this.radius = radius;

        angle = Randomizer.nextDouble(-10.0, 10.0, 0.1);
        angularSpeed = Randomizer.nextInt(-90, 90);

        generatePoints();
    }

    private void generatePoints() {
        int vertices = Randomizer.nextInt(4, 6);

        points = new ArrayList<>(vertices);
        double pointAngle = angle;
        double angleStep = 360.0 / vertices;

        int randomStepMax = (int)(angleStep * 0.9);
        for(int i=0; i<vertices; ++i) {
            double randomStep = Randomizer.nextInt(-randomStepMax/2, randomStepMax/2);
            double radians = Math.toRadians(pointAngle + randomStep);
            double x = Math.cos(radians) * radius;
            double y = Math.sin(radians) * radius;
            points.add(new Point2f(x,y));

            pointAngle += angleStep;
        }
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {
        g.setColor(Color.WHITE);

        Point2f center = new Point2f(0,0);
        Point2f previous = points.get(0);

        for(int i=1; i<points.size() + 1; ++i) {
            Point2f next;
            if(i == points.size())
                next = points.get(0);
            else
                next = points.get(i);

            double radians = Math.toRadians(angle);
            Point2f prevRotated = rotatePoint(previous, center, radians);
            Point2f nextRotated = rotatePoint(next, center, radians);

            Shape line = new Line2D.Double(prevRotated.getX() + x, prevRotated.getY() + y, nextRotated.getX() + x, nextRotated.getY() + y);
            g.setStroke(new BasicStroke(2));
            g.draw(line);

            previous = next;
        }
    }

    @Override
    public void updatePhysics(double timeSinceLastUpdate) {
        super.updatePhysics(timeSinceLastUpdate);
        angle += angularSpeed * timeSinceLastUpdate;
    }

    @Override
    public boolean shouldDelete() {
        return y - radius > GameWindow.GAME_SIZE.height;
    }

    public void setAngularSpeed(double angularSpeed) {
        this.angularSpeed = angularSpeed;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    public ArrayList<Point2f> getPoints() {
        return points;
    }
}
