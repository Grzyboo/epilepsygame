package grzyboo.entities;

public interface PointFunctions {
    default double distance(Point2f p1, Point2f p2) {
        return Math.sqrt((p1.x - p2.x)*(p1.x - p2.x) + (p1.y - p2.y)*(p1.y - p2.y));
    }

    default double vectorAngle(Point2f p1, Point2f p2) {
        return Math.atan2(p2.y - p1.y, p2.x - p1.x);
    }

    default Point2f rotatePoint(Point2f p1, Point2f center, double angle) {
        double x1 = p1.getX() - center.getX();
        double y1 = p1.getY() - center.getY();

        double temp_x1 = x1 * Math.cos(angle) - y1 * Math.sin(angle);
        double temp_y1 = x1 * Math.sin(angle) + y1 * Math.cos(angle);

        return new Point2f(temp_x1 + center.getX(), temp_y1 + center.getY());
    }
}
