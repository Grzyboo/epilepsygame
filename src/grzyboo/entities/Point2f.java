package grzyboo.entities;

public class Point2f implements PointFunctions {
    protected double x, y;

    public Point2f(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point2f() {
        x = y = 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setPosition(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setPosition(Point2f p) {
        x = p.x;
        y = p.y;
    }

    public Point2f add(Point2f p) {
        return new Point2f(x + p.x, y + p.y);
    }
}
