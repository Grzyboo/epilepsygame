package grzyboo.entities;

import grzyboo.GamePanel;
import grzyboo.Resources;

import java.awt.*;
import java.awt.image.BufferedImage;

public class GiantHeart extends Entity {
    private final double startingWidth = 400;
    private double width;
    private double shrinkSpeed;

    private float alpha;

    public GiantHeart() {
        width = startingWidth;
        shrinkSpeed = -1500.0;
        alpha = 0.6f;
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {
        if((int)width > 0) {
            PlayerCursor cursor = GamePanel.getInstance().getPlayerCursor();

            Composite oldComposite = g.getComposite();
            Composite newComposite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha);
            g.setComposite(newComposite);

            g.drawImage(getCurrentImage(), (int) (cursor.getX() - width * 0.5), (int) (cursor.getY() - width * 0.5), null);

            g.setComposite(oldComposite);
        }
    }

    @Override
    public void updatePhysics(double timeSinceLastUpdate) {
        alpha -= 0.5 * timeSinceLastUpdate;
        width -= shrinkSpeed * timeSinceLastUpdate;

        if(width >= 1.5*startingWidth)
            shrinkSpeed = 2500;
    }

    @Override
    public boolean shouldDelete() {
        return alpha <= 0.0 || width <= 0.0;
    }

    private Image getCurrentImage() {
        BufferedImage original = Resources.IMG_HEART;
        BufferedImage resized = new BufferedImage((int)width, (int)width, original.getType());
        Graphics2D g = resized.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(original, 0, 0, (int)width, (int)width, 0, 0, original.getWidth(), original.getHeight(), null);
        g.dispose();

        return resized;
    }
}
