package grzyboo.entities;

import grzyboo.GamePanel;

import java.awt.*;
import java.awt.geom.Arc2D;

public class Laser extends Entity {
    private boolean exploded = false;

    private double maxRadius = 25.0f;
    private double expansionSpeed = 8.0f;
    private double radius = 1.0f;

    public Laser() {
        GamePanel panel = GamePanel.getInstance();
        Dimension size = panel.getSize();

        if(Randomizer.nextBoolean())
            x = 25f;
        else
            x = size.getWidth() - 25.0;

        y = Randomizer.nextFloat(25f, (float)size.getHeight() - 25f);
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {
        try {
            int alpha = (int)Math.round(radius / maxRadius * 255);
            g.setColor(new Color(255, 0, 0, alpha));
        } catch(IllegalArgumentException e) {
            System.out.println("Laser alpha = " + (Math.round(radius / maxRadius * 255)) + " //// " + radius + " / " + maxRadius);
        }

        if(exploded) {
            Dimension size = GamePanel.getInstance().getSize();
            g.fillRect(-25, (int)(y - radius), (int)size.getWidth() + 25, (int)(radius * 2));
        }
        else {
            Arc2D circle = new Arc2D.Double(x - radius, y - radius, radius*2, radius*2, 0, 360, Arc2D.CHORD);
            g.fill(circle);
        }
    }

    @Override
    public void updatePhysics(double timeSinceLastUpdate) {
        radius += expansionSpeed * timeSinceLastUpdate;
        if(radius >= maxRadius)
            explode();
    }

    @Override
    public boolean shouldDelete() {
        return radius < 0.0;
    }

    private void explode() {
        exploded = true;
        expansionSpeed  = -40.0f;
        radius *= 0.75f;

        PlayerCursor cursor = GamePanel.getInstance().getPlayerCursor();
        if(Math.abs(cursor.getY() - y) < cursor.getRadius() + radius)
            cursor.hit();
    }
}
