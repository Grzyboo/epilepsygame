package grzyboo.entities;

import grzyboo.GameWindow;
import grzyboo.Resources;

import java.awt.*;
import java.util.ArrayList;

public abstract class PowerUp extends Entity {
    private ArrayList<Point2f> points;
    public static final double RADIUS = 24.0f;

    protected abstract void onCursorTouch();

    private boolean delete;

    public PowerUp() {
        final int numberOfPoints = 10;
        points = new ArrayList<>(numberOfPoints);
        delete = false;

        double angleStep = 360.0f / numberOfPoints;
        for(int i = 0; i < numberOfPoints; ++i) {
            double angle = Math.toRadians(i * angleStep + angleStep/2);

            double radius = RADIUS;
            if(i % 2 != 1)
                radius = RADIUS / 2;

            Point2f point = new Point2f(Math.cos(angle) * radius, Math.sin(angle) * radius);
            points.add(point);
        }

        setPositionProperties();
    }

    private void setPositionProperties() {
        boolean side = Randomizer.nextBoolean();
        boolean vertical = Randomizer.nextBoolean();

        if(vertical) {
            x = Randomizer.nextDouble(RADIUS, GameWindow.GAME_SIZE.getWidth() - RADIUS);
            y = -RADIUS;

            double speedX = Randomizer.nextDouble(-200, 200, 50);
            double speedY = 500.0;
            setSpeed(speedX, speedY);
        }
        else {
            x = side ? GameWindow.GAME_SIZE.getWidth() + RADIUS : -RADIUS;
            y = GameWindow.GAME_SIZE.getHeight() * 0.2;

            double speedX = side ? -500.0f : 500.0f;
            double speedY = Randomizer.nextInt(2, 10) * -10.0;
            setSpeed(speedX, speedY);
        }
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {
        int n = points.size();
        int[] xPoints = new int[n];
        int[] yPoints = new int[n];

        for(int i = 0; i < n; ++i){
            Point2f point = points.get(i);
            xPoints[i] = (int)(point.getX() + x);
            yPoints[i] = (int)(point.getY() + y);
        }

        Polygon polygon = new Polygon(xPoints, yPoints, n);
        g.setColor(Color.YELLOW);
        g.fill(polygon);
        g.setColor(Color.ORANGE);
        g.draw(polygon);
    }

    @Override
    public boolean shouldDelete() {
        return y - RADIUS > GameWindow.GAME_SIZE.height || delete;
    }

    public void cursorTouched() {
        onCursorTouch();
        Resources.PICKUP.play();
        delete = true;
    }
}
