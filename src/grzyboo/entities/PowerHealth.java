package grzyboo.entities;

import grzyboo.GamePanel;

public class PowerHealth extends PowerUp {
    @Override
    protected void onCursorTouch() {
        GamePanel panel =  GamePanel.getInstance();
        panel.getPlayerCursor().incrementHealth();
        panel.addEntity(new GiantHeart());
    }
}
