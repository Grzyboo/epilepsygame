package grzyboo.entities;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Particle extends Entity {
    public Particle(Point2f position, boolean gravity) {
        x = position.x;
        y = position.y;
        this.gravity = gravity;
    }

    public void draw(Graphics2D g, double radius, Color color) {
        Shape circle = new Ellipse2D.Double(x, y, radius, radius);
        g.setColor(color);
        g.fill(circle);
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {}

    @Override
    public boolean shouldDelete() {
        return false;
    }

    @Override
    public void updatePhysics(double timeSinceLastUpdate) {
        super.updatePhysics(timeSinceLastUpdate);
    }
}
