package grzyboo.entities;

import java.awt.*;
import java.util.ArrayList;

public class ParticlePack extends Entity {
    private java.util.List<Particle> particles;
    private double radius;
    private double shrinkSpeed;
    protected Color color;

    private ParticlePack() {}

    public static ParticlePack generateSmall(Point2f start, int amount, Color color) {
        ParticlePack pack = new ParticlePack();

        pack.radius = 5.0;
        pack.shrinkSpeed = 2.5;
        pack.color = color;
        pack.particles = new ArrayList<>(amount);

        for(int i = 0; i < amount; ++i) {
            Particle p = new Particle(start, true);
            p.setSpeed(Randomizer.nextDouble(-25, 25), Randomizer.nextDouble(-100, 100));
            pack.particles.add(p);
        }

        return pack;
    }

    public static ParticlePack generateBoom(Point2f start, int amount, Color color) {
        ParticlePack pack = new ParticlePack();

        pack.radius = 7.0;
        pack.shrinkSpeed = 5.0;
        pack.color = color;
        pack.particles = new ArrayList<>(amount);

        for(int i = 0; i < amount; ++i) {
            Particle p = new Particle(start, false);
            double speed = Randomizer.nextInt(-50, 50) + 600;
            double angle = Math.toRadians(Randomizer.nextDouble(0, 360, 0.001));
            p.setSpeed(speed*Math.cos(angle), speed*Math.sin(angle));
            pack.particles.add(p);
        }

        return pack;
    }

    public int getAmount() {
        return particles.size();
    }

    @Override
    public void drawOnCanvas(Graphics2D g) {
        for(Particle p : particles)
            p.draw(g, radius, color);
    }

    @Override
    public void updatePhysics(double timeSinceLastUpdate) {
        radius -= shrinkSpeed * timeSinceLastUpdate;

        for(Particle p : particles)
            p.updatePhysics(timeSinceLastUpdate);
    }

    @Override
    public boolean shouldDelete() {
        return radius <= 0.0;
    }
}
