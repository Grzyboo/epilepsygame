package grzyboo.entities;

import grzyboo.GamePanel;

public class PowerImmortality extends PowerUp {
    @Override
    protected void onCursorTouch() {
        GamePanel.getInstance().getPlayerCursor().setImmortal(3000);
    }
}
