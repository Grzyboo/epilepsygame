package grzyboo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

public class GameWindow extends JFrame {
    public static final Dimension GAME_SIZE = new Dimension(800, 600);
    private GamePanel mainPanel;

    public GameWindow() {
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        Resources.load();

        mainPanel = GamePanel.getInstance();
        mainPanel.setPreferredSize(GAME_SIZE);
        mainPanel.setBorder(BorderFactory.createEtchedBorder());
        add(mainPanel);

        mainPanel.setParentFrame(this);

        hideMainPanelCursor();
        createComponents();

        addKeyListener(mainPanel);

        setResizable(false);
    }

    private void createComponents() {
        JPanel infoPanel = new JPanel();
        infoPanel.add(new JLabel("Made by Grzyboo @ 2018"));

        add(infoPanel);
    }

    private void hideMainPanelCursor() {
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");
        mainPanel.setCursor(blankCursor);
    }
}
