package grzyboo;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new GameWindow();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
