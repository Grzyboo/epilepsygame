package grzyboo.menu;

import grzyboo.GamePanel;
import grzyboo.Resources;
import grzyboo.menu.options.*;

import java.awt.*;

public class EndMenu extends GameMenu {
    private int score;

    public EndMenu(double score) {
        super(new MenuOption[] {
                new StartNewGameOption("Replay!"),
                new ExitOption()
        }, 1);

        this.score = (int)score;
    }

    @Override
    public void draw(Graphics2D g) {
        g.setFont(Resources.FONT);
        g.setColor(Color.ORANGE);

        FontMetrics fm = g.getFontMetrics();
        Dimension panelSize = GamePanel.getInstance().getSize();

        String text = "SCORE: " + score;
        int x = (int)(panelSize.getWidth() - fm.stringWidth(text))/2;
        int y = (int)(0.1*panelSize.getHeight());
        g.drawString(text, x, y);
        
        super.draw(g);
    }
}
