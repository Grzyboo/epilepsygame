package grzyboo.menu;

public enum DifficultyLevels {
    EASY(800, 3000, 3),
    MEDIUM(500, 5000, 7),
    HARD(300, 10_000, 15);

    private int timeForNextAsteroid;
    private int timeForNextPowerUp;
    private int pointsPerSecond;

    DifficultyLevels(int timeForNextAsteroid, int timeForNextPowerUp, int pointsPerSecond) {
        this.timeForNextAsteroid = timeForNextAsteroid;
        this.timeForNextPowerUp = timeForNextPowerUp;
        this.pointsPerSecond = pointsPerSecond;
    }

    public int getTimeForNextAsteroid() {
        return timeForNextAsteroid;
    }

    public int getTimeForNextPowerUp() {
        return timeForNextPowerUp;
    }

    public int getPointsPerSecond() {
        return pointsPerSecond;
    }
}
