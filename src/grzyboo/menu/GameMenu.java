package grzyboo.menu;

import grzyboo.Resources;
import grzyboo.menu.options.MenuOption;

import java.awt.*;
import java.util.ArrayList;

public class GameMenu {
    private java.util.List<MenuOption> menuOptions;
    private int currentItem = 0;

    GameMenu(MenuOption[] options, int startPos) {
        menuOptions = new ArrayList<>();

        int pos = startPos;
        for(MenuOption option : options) {
            menuOptions.add(option);
            option.setPosition(pos);
            ++pos;
        }
    }

    GameMenu(MenuOption[] options) {
        this(options, 0);
    }

    public void draw(Graphics2D g) {
        g.setFont(Resources.FONT);

        for(int i=0; i<menuOptions.size(); ++i) {
            MenuOption option = menuOptions.get(i);

            if(currentItem == i)
                option.draw(g, Color.CYAN);
            else
                option.draw(g, Color.GRAY);
        }
    }

    public void nextItem() {
        currentItem = ++currentItem % menuOptions.size();
        Resources.MENU.play();
    }

    public void previousItem() {
        currentItem = --currentItem % menuOptions.size();

        if(currentItem == -1)
            currentItem += menuOptions.size();

        Resources.MENU.play();
    }

    public void useCurrentlySelectedItem() {
        MenuOption option = menuOptions.get(currentItem);
        option.use();
        Resources.MENU_CLICK.play();
    }


}
