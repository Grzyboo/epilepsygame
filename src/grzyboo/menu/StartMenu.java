package grzyboo.menu;

import grzyboo.menu.options.*;

public class StartMenu extends GameMenu {

    public StartMenu() {
        super(new MenuOption[] {
                new StartNewGameOption("Start game"),
                new DifficultyOption(),
                new ExitOption()
        });
    }
}
