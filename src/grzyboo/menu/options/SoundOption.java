package grzyboo.menu.options;

import grzyboo.Settings;

public class SoundOption extends MenuOption {
    public SoundOption() {
        super("Sound: " + (Settings.getSoundsON() ? "ON" : "OFF"));
    }

    @Override
    public void use() {
        Settings.setSoundsON(!Settings.getSoundsON());
        setText("Sound: " + (Settings.getSoundsON() ? "ON" : "OFF"));
    }
}
