package grzyboo.menu.options;

import grzyboo.GamePanel;
import grzyboo.Settings;

public class DifficultyOption extends MenuOption {
    public DifficultyOption() {
        super("Difficulty: " + Settings.getDifficulty().name());
    }

    @Override
    public void use() {
        Settings.nextDifficulty();
        setText("Difficulty: " + Settings.getDifficulty().name());
        GamePanel.getInstance().updateDifficulty();
    }
}
