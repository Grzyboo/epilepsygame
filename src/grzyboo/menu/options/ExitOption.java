package grzyboo.menu.options;

import grzyboo.GamePanel;

public class ExitOption extends MenuOption {

    public ExitOption() {
        super("Exit");
    }

    @Override
    public void use() {
        GamePanel.getInstance().exit();
    }
}
