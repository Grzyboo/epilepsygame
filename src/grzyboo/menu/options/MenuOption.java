package grzyboo.menu.options;

import grzyboo.GamePanel;

import java.awt.*;

public abstract class MenuOption  {
    private String text;
    private int position;

    public MenuOption(String text) {
        this.text = text;
        this.position = 0;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void draw(Graphics2D g, Color color) {
        g.setColor(color);

        FontMetrics fm = g.getFontMetrics();
        Dimension panelSize = GamePanel.getInstance().getSize();
        int x = (int)(panelSize.getWidth() - fm.stringWidth(text))/2;
        int y = (int)(0.2*panelSize.getHeight() + fm.getHeight()*position);
        g.drawString(text, x, y);
    }

    void setText(String text) {
        this.text = text;
    }

    public abstract void use();


}
