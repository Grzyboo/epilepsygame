package grzyboo.menu.options;

import grzyboo.GamePanel;

public class PlayOption extends MenuOption {
    public PlayOption() {
        super("Resume");
    }

    @Override
    public void use() {
        GamePanel.getInstance().unpause();
    }
}
