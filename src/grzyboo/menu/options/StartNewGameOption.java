package grzyboo.menu.options;

import grzyboo.GamePanel;

public class StartNewGameOption extends MenuOption {
    public StartNewGameOption(String name) {
        super(name);
    }

    @Override
    public void use() {
        GamePanel.getInstance().newGame();
    }
}
