package grzyboo.menu;

import grzyboo.menu.options.*;

public class PauseMenu extends GameMenu {
    public PauseMenu() {
        super(new MenuOption[] {
                new PlayOption(),
                new SoundOption(),
                new DifficultyOption(),
                new ExitOption()
        });
    }
}