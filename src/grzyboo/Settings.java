package grzyboo;

import grzyboo.menu.DifficultyLevels;

public class Settings {
    private static DifficultyLevels difficulty = DifficultyLevels.MEDIUM;
    private static boolean sounds = true;

    // For testing only:
    public static final boolean PERMANENT_IMMORTALITY = false; // Game won't end - the ending screen won't show.
    public static final boolean DISPLAY_ADDITIONAL_INFO = false; // Displays information like amount of entities / particles on the map.
    public static final double SLOW = 1.0; // Time multiplier

    public static boolean getSoundsON() {
        return sounds;
    }

    public static void setSoundsON(boolean on) {
        sounds = on;

        if(sounds)
            Resources.MUSIC.play();
        else
            Resources.MUSIC.stop();
    }

    public static DifficultyLevels getDifficulty() {
        return difficulty;
    }

    public static void nextDifficulty() {
        DifficultyLevels[] values = DifficultyLevels.values();

        for(int i=0; i < values.length; ++i) {
            if(difficulty == values[i]) {
                difficulty = values[(i + 1) % values.length];
                break;
            }
        }
    }
}
