package grzyboo;

public class SimpleTimer {
    private double time;
    private double timeLeft;
    private Runnable function;

    private boolean repeatable;
    private boolean absolute;
    private boolean enabled;

    /**
     *
     * @param time delay between next calls
     * @param function what should be done by the timer
     * @param repeatable true if timer should keep on repeating after first run
     */
    public SimpleTimer(double time, Runnable function, boolean repeatable) {
        this.time = time;
        this.function = function;
        this.repeatable = repeatable;
        this.enabled = true;
        timeLeft = time;
        absolute = false;
    }

    /**
     *
     * @param set if set, the timer will ignore any game pace changes. 3 second timer will be executed after 3 seconds no matter what.
     */
    public void setAbsolute(boolean set) {
        absolute = set;
    }

    /**
     *
     * @param timePassed time passed since last update
     * @param timeMultiplier if the time is slowed down timePassed should be multiplied as well if the timer isn't absolute
     * @return true if action was run
     */
    boolean update(double timePassed, double timeMultiplier) {
        if(enabled) {
            timeLeft -= timePassed * (absolute ? 1.0 : timeMultiplier);

            if (timeLeft <= 0.0) {
                function.run();
                timeLeft = time;
                return true;
            }
        }

        return false;
    }

    public void setTime(double ms) {
        time = ms;
    }

    public boolean isRepeatable() {
        return repeatable;
    }

    public void disable() {
        enabled = false;
    }

    public void enable() {
        enabled = true;
    }
}
