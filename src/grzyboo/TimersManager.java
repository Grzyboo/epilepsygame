package grzyboo;

import java.util.LinkedList;

public class TimersManager {
    private double lastUpdate;
    private LinkedList<SimpleTimer> timers;

    public TimersManager() {
        lastUpdate = System.currentTimeMillis();
        timers = new LinkedList<>();
    }

    public void addTimer(SimpleTimer timer) {
        timers.add(timer);
    }

    SimpleTimer addTimer(int msDelay, Runnable function) {
        SimpleTimer timer = new SimpleTimer(msDelay, function, true);
        timers.add(timer);

        return timer;
    }

    SimpleTimer addTimer(int msDelay, Runnable function, boolean repeatable) {
        SimpleTimer timer = new SimpleTimer(msDelay, function, repeatable);
        timers.add(timer);

        return timer;
    }

    public double update(double timeMultiplier) {
        double now = System.currentTimeMillis();
        double passed = now - lastUpdate;
        lastUpdate = now;

        for(int i = 0; i < timers.size(); ++i) {
            SimpleTimer timer = timers.get(i);
            if(timer.update(passed, timeMultiplier)) {
                if(!timer.isRepeatable()) {
                    timers.remove(i);
                    --i;
                }
            }
        }

        return passed * 0.001 * timeMultiplier;
    }

    public double getTimeWithoutUpdate(double timeMultiplier) {
        double now = System.currentTimeMillis();
        double passed = now - lastUpdate;
        lastUpdate = now;

        return passed * 0.001 * timeMultiplier;
    }

     public void stopAll() {
        timers.clear();
    }
}
