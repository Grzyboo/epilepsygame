package grzyboo;

import grzyboo.entities.*;

import java.awt.*;
import java.awt.geom.Line2D;
import java.util.List;

public class CollisionChecker implements PointFunctions {
    private Asteroid asteroid;
    private PlayerCursor cursor;

    void setObjects(PlayerCursor cursor, Asteroid asteroid) {
        this.cursor = cursor;
        this.asteroid = asteroid;
    }

    boolean collides() {
        if(asteroid == null || cursor == null)
            return false;

        if(distance(asteroid, cursor) > asteroid.getRadius() + cursor.getRadius())
            return false;

        if(distance(asteroid, cursor) < cursor.getRadius())
            return true;

        List<Point2f> points = asteroid.getPoints();
        Point2f previous = points.get(0);

        for(int i=1; i<points.size() + 1; ++i) {
            Point2f next;
            if (i == points.size())
                next = points.get(0);
            else
                next = points.get(i);

            if(checkSegment(previous, next))
                return true;

            previous = next;
        }

        return false;
    }

    private boolean checkSegment(Point2f start, Point2f end) {
        double radians = Math.toRadians(asteroid.getAngle());
        Point2f center = new Point2f(0,0);
        Point2f prevRotated = rotatePoint(start, center, radians).add(asteroid);
        Point2f nextRotated = rotatePoint(end, center, radians).add(asteroid);

        // y = ax + b   - This line represents a line that includes given segment
        double a = (prevRotated.getY() - nextRotated.getY()) / (prevRotated.getX() - nextRotated.getX());
        double b = prevRotated.getY() - a * prevRotated.getX();

        // Check if cursor intersects the segment
        double cursorDistanceToLine = Math.abs(a*cursor.getX() - cursor.getY() + b) / Math.sqrt(a*a + 1);
        if(cursorDistanceToLine < cursor.getRadius()) {
            // Check if intersection points are within circle collision radius
            double _a = 1 + a*a;
            double _b = -2*(cursor.getX() - a*b + a*cursor.getY());
            double _c = cursor.getX()*cursor.getX() + b*b - 2*cursor.getY()*b + cursor.getY()*cursor.getY() - cursor.getRadius()*cursor.getRadius();
            QuadraticEquation eq = new QuadraticEquation(_a, _b, _c);
            if(eq.delta() >= 0.0) {
                double _x1 = eq.x1();
                double _x2 = eq.x2();
                double _y1 = a*_x1 + b;
                double _y2 = a*_x2 + b;

                Point2f intersection1 = new Point2f((double)_x1, (double)_y1);
                Point2f intersection2 = new Point2f((double)_x2, (double)_y2);

                if(distance(intersection1, asteroid) <= asteroid.getRadius() || distance(intersection2, asteroid) <= asteroid.getRadius())
                    return true;
            }
        }

        // y = cx + d   - This line represents a line that goes through both asteroid and cursor
        double c = (cursor.getY() - asteroid.getY()) / (cursor.getX() - asteroid.getX());
        double d = cursor.getY() - c * cursor.getX();

        double intersectionX = (d - b) / (a - c);
        double intersectionY = a * intersectionX + b;
        Point2f intersection = new Point2f(intersectionX, intersectionY);


        // Check if cursor is inside the asteroid
        double asteroidToIntersectionDistance = distance(asteroid, intersection);
        if(asteroid.getRadius() >= asteroidToIntersectionDistance) {
            if(isPointBetween(cursor, intersection, asteroid))
                return true;
        }

        return false;
    }

    private boolean isPointBetween(Point2f checked, Point2f p1, Point2f p2) {
        double result = (distance(checked, p1) + distance(checked, p2)) - distance(p1, p2);
        return Math.abs(result) < 0.0001;
    }
}
