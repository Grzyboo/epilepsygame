package grzyboo.colors;

import java.awt.*;

public abstract class ColorChanger {
    protected double[] currentColor = new double[3];
    protected double[] colorChangeSpeed = new double[3];

    private double changeTime;
    private double timer;

    public ColorChanger(double changeTime) {
        this.changeTime = changeTime;
        timer = 0;

        currentColor = getStartingColor();
        createColorTransition();
    }

    private void createColorTransition() {
        double[] color = getNextColor();
        for(int i=0; i<3; ++i) {
            double difference = color[i] - currentColor[i];
            colorChangeSpeed[i] = difference / changeTime;
        }

        timer = changeTime;
    }

    public void update(double timeSinceLastUpdate) {
        timer -= timeSinceLastUpdate;
        if(timer <= 0.0)
            createColorTransition();

        for(int i=0; i<3; ++i)
            currentColor[i] += colorChangeSpeed[i] * timeSinceLastUpdate;
    }

    public Color getColor() {
        int r = (int)currentColor[0];
        int g = (int)currentColor[1];
        int b = (int)currentColor[2];
        r = r > 255 ? 255 : (r < 0 ? 0 : r);
        g = g > 255 ? 255 : (g < 0 ? 0 : g);
        b = b > 255 ? 255 : (b < 0 ? 0 : b);
        return new Color(r, g, b);
    }

    protected abstract double[] getNextColor();
    protected abstract double[] getStartingColor();
}
