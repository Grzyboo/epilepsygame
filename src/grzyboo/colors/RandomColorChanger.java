package grzyboo.colors;

import java.util.Random;

public class RandomColorChanger extends ColorChanger {

    public RandomColorChanger(double changeTime) {
        super(changeTime);
    }

    @Override
    protected double[] getNextColor() {
        Random r = new Random();
        return new double[] {r.nextInt(256), r.nextInt(256), r.nextInt(256)};
    }

    @Override
    protected double[] getStartingColor() {
        return getNextColor();
    }
}
