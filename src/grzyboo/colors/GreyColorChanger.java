package grzyboo.colors;

public class GreyColorChanger extends ColorChanger {

    public GreyColorChanger(double changeTime) {
        super(changeTime);
    }

    @Override
    protected double[] getNextColor() {
        if(colorChangeSpeed[0] < 0)
            return new double[]{70, 70, 70};
        else
            return new double[]{40, 40, 40};
    }

    @Override
    protected double[] getStartingColor() {
        return new double[]{70, 70, 70};
    }
}
