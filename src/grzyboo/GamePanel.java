package grzyboo;

import grzyboo.colors.ColorChanger;
import grzyboo.colors.GreyColorChanger;
import grzyboo.entities.*;
import grzyboo.menu.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Optional;

public class GamePanel extends JPanel implements KeyListener, PointFunctions {
    private java.util.List<Entity> entities = new ArrayList<>(25);
    private java.util.List<ParticlePack> particles = new ArrayList<>(300);

    private ColorChanger backgroundColorChanger;
    private CollisionChecker collisionChecker;
    private PlayerCursor cursor;
    private DifficultyLevels difficulty;
    private EntitiesFactory factory;

    public enum States {START, GAME, PAUSE, END}
    private States gameState;

    @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
    private Optional<GameMenu> currentlyOpenedMenu;
    private JFrame parentFrame;

    private TimersManager timersManager;
    private Timer mainTimer;
    private SimpleTimer asteroidsTimer;
    private SimpleTimer antiCampingAsteroidTimer;
    private SimpleTimer powerUpsTimer;

    private double playingTime;
    private double score;
    private double slowDownRatio;

    private static GamePanel instance; // There is always only 1 GamePanel so lets make it singleton for simplicity
    private GamePanel() {
        cursor = new PlayerCursor();
        backgroundColorChanger = new GreyColorChanger(0.8f);
        collisionChecker = new CollisionChecker();
        factory = new EntitiesFactory();

        gameState = States.START;
        currentlyOpenedMenu = Optional.of(new StartMenu());

        playingTime = 0.0;
        score = 0.0;
        slowDownRatio = 1.0;
        difficulty = DifficultyLevels.MEDIUM;

        createMainTimer();
        createMouseListeners();
    }

    public static GamePanel getInstance() {
        if(instance == null)
            instance = new GamePanel();

        return instance;
    }

    void setParentFrame(JFrame parentFrame) {
        this.parentFrame = parentFrame;
    }

    private void createMainTimer() {
        int delay = 15;
        ActionListener taskPerformer = event -> mainLoop();
        mainTimer = new Timer(delay, taskPerformer);
        mainTimer.start();

        timersManager = new TimersManager();
    }

    private void mainLoop() {
        update();
        repaint();
    }

    private void createTimers() {
        asteroidsTimer = timersManager.addTimer(400, () -> entities.add(factory.createAsteroid()));
        antiCampingAsteroidTimer = timersManager.addTimer(1000, () -> entities.add(factory.createAntiCampingAsteroid(cursor)));
        powerUpsTimer = timersManager.addTimer(4000, () -> entities.add(factory.createPowerUp(cursor, playingTime)));
        timersManager.addTimer(3000, () -> entities.add(new Laser()));
    }

    public void addTimer(SimpleTimer timer) {
        timersManager.addTimer(timer);
    }

    private void createMouseListeners() {
        addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                cursor.setPosition(e.getX(), e.getY());
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                cursor.setPosition(e.getX(), e.getY());
            }
        });
    }

    @Override
    public void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        Graphics2D g = (Graphics2D) graphics;

        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Composite oldComposite = g.getComposite();
        if(gameState != States.GAME)
            g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, .2f));

        for(ParticlePack p : particles)
            p.drawOnCanvas(g);

        for(Entity e : entities)
            e.drawOnCanvas(g);

        cursor.drawOnCanvas(g);
        displayInfo(g);

        g.setComposite(oldComposite);

        currentlyOpenedMenu.ifPresent(menu -> menu.draw(g));
        g.setStroke(new BasicStroke(0)); // Don't show canvas border
    }

    private void update() {
        double timePassed;
        if(gameState == States.GAME)
            timePassed = timersManager.update(slowDownRatio * Settings.SLOW);
        else
            timePassed = timersManager.getTimeWithoutUpdate(slowDownRatio * Settings.SLOW);

        for(int i = 0; i < particles.size(); ++i) {
            ParticlePack p = particles.get(i);
            p.updatePhysics(timePassed);

            if(p.shouldDelete())
                particles.remove(i--);
        }

        switch(gameState) {
            case START:
            case PAUSE:
            case END: {
                for (Entity e : entities) {
                    if(e instanceof PowerUp)
                        particles.add(ParticlePack.generateSmall(e, 4, Color.YELLOW));
                }

                cursor.updatePhysics(timePassed);
                backgroundColorChanger.update(timePassed);
                setBackground(backgroundColorChanger.getColor());

                particles.add(ParticlePack.generateSmall(cursor, 10, cursor.getColor()));
                break;
            }

            case GAME: {
                for (int i = 0; i < entities.size(); ++i) {
                    Entity e = entities.get(i);
                    e.updatePhysics(timePassed);

                    if(e instanceof Asteroid) {
                        collisionChecker.setObjects(cursor, (Asteroid)e);
                        if(collisionChecker.collides())
                            cursor.hit();
                    }

                    if(e instanceof PowerUp) {
                        particles.add(ParticlePack.generateSmall(e, 4, Color.YELLOW));

                        if (distance(e, cursor) < cursor.getRadius() + PowerUp.RADIUS) {
                            PowerUp p = (PowerUp)e;
                            p.cursorTouched();
                        }
                    }

                    if(e.shouldDelete())
                        entities.remove(i--);
                }

                cursor.updatePhysics(timePassed);
                backgroundColorChanger.update(timePassed);
                setBackground(backgroundColorChanger.getColor());

                score += timePassed * difficulty.getPointsPerSecond();

                particles.add(ParticlePack.generateSmall(cursor, 10, cursor.getColor()));
                break;
            }
        }
    }

    private void displayInfo(Graphics2D g) {
        g.setFont(Resources.DEFAULT_FONT);

        if(gameState == States.GAME) {
            g.setColor(Color.ORANGE);
            g.drawString("Score: " + ((int) score), 20, 60);
        }

        if(Settings.DISPLAY_ADDITIONAL_INFO) {
            int _particlesPacks = particles.size();
            int _particles = 0;
            for(ParticlePack pack : particles)
                _particles += pack.getAmount();

            int _asteroids = 0;
            int _powerups = 0;

            for (Entity e : entities) {
                if (e instanceof Asteroid)
                    ++_asteroids;
                else if (e instanceof PowerUp)
                    ++_powerups;
            }

            g.setColor(Color.WHITE);
            g.drawString("Entities: " + entities.size(), 5, 80);
            g.drawString("  ParticlesPacks: " + _particlesPacks, 5, 100);
            g.drawString("  Particles: " + _particles, 5, 120);
            g.drawString("  Asteroids: " + _asteroids, 5, 140);
            g.drawString("  Powerups: " + _powerups, 5, 160);
        }
    }

    public void makeBoomPowerUp() {
        for(Entity e : entities) {
            if(e instanceof Asteroid) {
                Asteroid asteroid = (Asteroid) e;
                double speed = 500.0f + Randomizer.nextFloat(0, 100, 10);

                double vecX = asteroid.getX() - cursor.getX();
                double vecY = asteroid.getY() - cursor.getY();
                double angle = Math.atan2(vecY, vecX);

                double speedX = Math.cos(angle) * speed;
                double speedY = Math.sin(angle) * speed;

                asteroid.setSpeed(speedX, speedY);

                asteroidsTimer.disable();
                antiCampingAsteroidTimer.disable();

                SimpleTimer timer = new SimpleTimer(1000, () -> {
                    asteroidsTimer.enable();
                    antiCampingAsteroidTimer.enable();
                }, false);

                addTimer(timer);
            }
        }

        particles.add(ParticlePack.generateBoom(cursor, 800, cursor.getColor()));
    }

    public PlayerCursor getPlayerCursor() {
        return cursor;
    }

    public void addEntity(Entity entity) {
        entities.add(entity);
    }

    public void setSlowDownRatio(double ratio) {
        slowDownRatio = ratio;
    }

    public void updateDifficulty() {
        DifficultyLevels level = Settings.getDifficulty();
        if(asteroidsTimer != null) {
            int asteroidTime = level.getTimeForNextAsteroid();
            asteroidsTimer.setTime(asteroidTime);
        }
        if(powerUpsTimer != null) {
            int powerupTime = level.getTimeForNextPowerUp();
            powerUpsTimer.setTime(powerupTime);
        }

    }

    public boolean shouldMoveCursor() {
        return gameState == States.GAME || gameState == States.START;
    }


    /*     Methods for changing game state      */
    public void newGame() {
        cursor.reset();
        unpause();
        Resources.MUSIC.play();

        entities.clear();
        particles.clear();

        score = 0;

        timersManager.stopAll();
        createTimers();
    }

    private void pause() {
        gameState = States.PAUSE;
        currentlyOpenedMenu = Optional.of(new PauseMenu());
    }

    public void unpause() {
        currentlyOpenedMenu = Optional.empty();
        gameState = States.GAME;
    }

    public void gameOver() {
        gameState = States.END;
        currentlyOpenedMenu = Optional.of(new EndMenu(score));

        Resources.MUSIC.stop();
        Resources.GAME_OVER.play();
    }

    public void exit() {
        currentlyOpenedMenu = Optional.empty();
        mainTimer.stop();
        parentFrame.dispose();
    }

    @Override public void keyTyped(KeyEvent e) {}
    @Override public void keyReleased(KeyEvent e) {}
    @Override
    public void keyPressed(KeyEvent e) {
        switch(e.getKeyCode()) {
            case KeyEvent.VK_ESCAPE: {
                if(gameState == States.PAUSE)
                    unpause();
                else if(gameState == States.GAME)
                    pause();

                break;
            }
            case KeyEvent.VK_UP:
                currentlyOpenedMenu.ifPresent(GameMenu::previousItem);
                break;
            case KeyEvent.VK_DOWN:
                currentlyOpenedMenu.ifPresent(GameMenu::nextItem);
                break;
            case KeyEvent.VK_ENTER:
                currentlyOpenedMenu.ifPresent(GameMenu::useCurrentlySelectedItem);
                break;
        }
    }

    @Override
    public Dimension getMaximumSize() {
        return GameWindow.GAME_SIZE;
    }
}
