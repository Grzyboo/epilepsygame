# EpilepsyGame

This is a simple game created using Swing only. I made it for fun and as a project for my classes at university.

Called it *EpilepsyGame* at first, because I expected a lot of flashing and sudden changes of color. As I've been adding new things my plans changed a bit and the game isn't as "flashy" as originally intended. There is some kind of blinking though, so it might be dangerous for people suffering from epilepsy.

The idea is to avoid asteroids, collect powerups and last as long as possible. There are 3 difficulty levels, which change frequency of new asteroids and powerups and score you will gain for every second played.

[Preview](https://drive.google.com/open?id=1ZU7NmHGZWKLFfNi94RA8CMBGyTGJ5q1i)

[Used resources: Files and sounds](https://drive.google.com/open?id=1jiIra2U-qx327YmI7uoUrV-iXGJeukhO)
